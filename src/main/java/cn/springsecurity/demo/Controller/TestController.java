package cn.springsecurity.demo.Controller;

import com.alibaba.fastjson.JSONObject;
import org.springframework.lang.Nullable;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import javax.validation.constraints.Null;

@Controller
public class TestController {

    public static volatile Integer countNumber =0;

    @RequestMapping("/")
    public String HomeIndexPage(){
        return "/Views/Index";
    }
    @RequestMapping("/loginPage")
    public String LoginPage(){
        return "/Views/Login";
    }

    @RequestMapping("/loginProc")
    public String loginProcAction(){
        System.out.println("come in Login processing");
        return "/";
    }
    @ResponseBody
    @RequestMapping("/sayhi/{id}")
    public JSONObject firstAction(@Null @PathVariable("id")String id){
        if(id==null)
            id = "001";
        JSONObject jsonObject = new JSONObject();
        jsonObject.put("Id",id);
        synchronized (countNumber) {
        jsonObject.put("number", ++countNumber);
            jsonObject.put("Action", "sayhi+id");
        }
        return jsonObject;
    }

    @ResponseBody
    @RequestMapping("/sayhello/{name}")
    public JSONObject secondAction(@Nullable @PathVariable("name")String id){
        if(id==null)
            id = "001";
        JSONObject jsonObject = new JSONObject();
        jsonObject.put("Id",id);
        synchronized (countNumber) {
            jsonObject.put("number", ++countNumber);
            jsonObject.put("Action", "sayhello+name");
        }
        return jsonObject;
    }

    @PreAuthorize("hasAnyRole('ROLE_ADMIN','ROLE_NORMAL')")
    @ResponseBody
    @RequestMapping("/goodbye/{name}")
    public JSONObject thirdAction(@PathVariable("name")String name){
        if(name==null)
            name = "001";
        JSONObject jsonObject = new JSONObject();
        jsonObject.put("Name",name);
        synchronized (countNumber) {
            jsonObject.put("number", ++countNumber);
            jsonObject.put("Action", "goodbyte+name");
        }
        return jsonObject;
    }

    @PreAuthorize("hasAnyAuthority('AUTH_PUSH','AUTH_POP')")
    @ResponseBody
    @RequestMapping("/push")
    public JSONObject ForthAction(){
        String name = "Someone";
        if(name==null)
            name = "001";
        JSONObject jsonObject = new JSONObject();
        jsonObject.put("Name",name);
        synchronized (countNumber) {
            jsonObject.put("number", ++countNumber);
            jsonObject.put("Action", "goodbyte+name");
        }
        return jsonObject;
    }

//    @PreAuthorize("hasAnyAuthority('AUTH_POP')")
    @ResponseBody
    @RequestMapping("/pop")
    public JSONObject thirdAction(){
        String name = "a person";
        if(name==null)
            name = "001";
        JSONObject jsonObject = new JSONObject();
        jsonObject.put("Name",name);
        synchronized (countNumber) {
            jsonObject.put("number", ++countNumber);
            jsonObject.put("Action", "goodbyte+name");
        }
        return jsonObject;
    }
}
