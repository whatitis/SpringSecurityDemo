package cn.springsecurity.demo.SecurityConfig;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.config.annotation.authentication.builders.AuthenticationManagerBuilder;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.AuthorityUtils;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.userdetails.User;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Service;

import java.util.LinkedList;
import java.util.List;

@Service
public class UserDetailsServer implements UserDetailsService {
    @Override
    public UserDetails loadUserByUsername(String username) throws UsernameNotFoundException {
        if(username.equals("aa")) {
            List<GrantedAuthority> grantedAuthorities = new LinkedList<>();
            ((LinkedList<GrantedAuthority>) grantedAuthorities).offer(new SimpleGrantedAuthority("ROLE_ADMIN"));
            return new org.springframework.security.core.userdetails.User(username, "698d51a19d8a121ce581499d7b701668", grantedAuthorities);
        } else if (username.equals("bb")) {
            List<GrantedAuthority> grantedAuthorities = new LinkedList<>();
            ((LinkedList<GrantedAuthority>) grantedAuthorities).offer(new SimpleGrantedAuthority("ROLE_NORMAL"));
            return new org.springframework.security.core.userdetails.User(username, "698d51a19d8a121ce581499d7b701668", grantedAuthorities);
        }else if (username.equals("cc")) {
            List<GrantedAuthority> grantedAuthorities = new LinkedList<>();
            ((LinkedList<GrantedAuthority>) grantedAuthorities).offer(new SimpleGrantedAuthority("AUTH_PUSH"));
            return new org.springframework.security.core.userdetails.User(username, "698d51a19d8a121ce581499d7b701668", grantedAuthorities);
        }else if (username.equals("dd")) {
            List<GrantedAuthority> grantedAuthorities = new LinkedList<>();
            ((LinkedList<GrantedAuthority>) grantedAuthorities).offer(new SimpleGrantedAuthority("AUTH_PUSH"));
            ((LinkedList<GrantedAuthority>) grantedAuthorities).offer(new SimpleGrantedAuthority("AUTH_POP"));
            return new org.springframework.security.core.userdetails.User(username, "698d51a19d8a121ce581499d7b701668", grantedAuthorities);
        }
        return null;
    }
}

